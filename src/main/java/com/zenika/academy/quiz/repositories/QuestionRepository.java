package com.zenika.academy.quiz.repositories;

import com.zenika.academy.quiz.domain.questions.Question;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class QuestionRepository {

    private Map<Long, Question> questionsById = new HashMap<>();

    public List<Question> getAll() {
        return List.copyOf(this.questionsById.values());
    }

    public void save(Question q) {
        this.questionsById.put(q.getId(), q);
    }

    public Optional<Question> getOne(long id) {
        return Optional.ofNullable(this.questionsById.get(id));
    }
}
