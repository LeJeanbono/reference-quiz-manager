package com.zenika.academy.quiz.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class TmdbConfiguration {

    @Value("classpath:tmdb-api-key.txt")
    private Resource tmdbApiKey;

}
