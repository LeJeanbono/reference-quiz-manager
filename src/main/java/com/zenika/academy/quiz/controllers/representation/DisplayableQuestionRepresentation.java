package com.zenika.academy.quiz.controllers.representation;

import com.zenika.academy.quiz.domain.questions.TypeQuestion;

import java.util.List;

public class DisplayableQuestionRepresentation {
    private String enonce;
    private long id;
    private List<String> suggestions;
    private TypeQuestion type;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    public TypeQuestion getType() {
        return type;
    }

    public void setType(TypeQuestion type) {
        this.type = type;
    }
}
