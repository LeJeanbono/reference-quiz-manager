package com.zenika.academy.quiz.controllers.representation;

import com.zenika.academy.quiz.domain.questions.TypeQuestion;

import java.util.List;

public class NewQuestionRepresentation {

    private Long id;

    private TypeQuestion type;

    private String enonce;

    private List<String> suggestions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeQuestion getType() {
        return type;
    }

    public void setType(TypeQuestion type) {
        this.type = type;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }
}
