package com.zenika.academy.quiz.controllers;

import com.zenika.academy.quiz.controllers.representation.AnswerResultRepresentation;
import com.zenika.academy.quiz.controllers.representation.NewQuestion;
import com.zenika.academy.quiz.controllers.representation.NewQuestionRepresentation;
import com.zenika.academy.quiz.controllers.representation.UserAnswerRepresentation;
import com.zenika.academy.quiz.domain.questions.*;
import com.zenika.academy.quiz.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping
    List<NewQuestionRepresentation> getAllQuestions() {
        return this.questionService.getAllQuestions().stream()
                .map(this::createDisplayableRepresentation)
                .collect(Collectors.toList());
    }

    private NewQuestionRepresentation createDisplayableRepresentation(Question q) {
        NewQuestionRepresentation nq = new NewQuestionRepresentation();
        nq.setId(q.getId());
        nq.setEnonce(q.getText());
        nq.setSuggestions(q.getSuggestions());
        if (q instanceof OpenQuestion) {
            nq.setType(TypeQuestion.OPEN);
        } else if (q instanceof TrueFalseQuestion) {
            nq.setType(TypeQuestion.TRUE_FALSE);
        } else if (q instanceof MultipleChoiceQuestion) {
            nq.setType(TypeQuestion.MULTIPLE);
        }
        return nq;
    }

    @GetMapping("/{id}")
    ResponseEntity<NewQuestionRepresentation> getOneQuestion(@PathVariable("id") long id) {
        Optional<Question> question = this.questionService.getOneQuestion(id);

        return question
                .map(this::createDisplayableRepresentation)
                .map(ResponseEntity::ok) // Optional<ResponseEntity<Question>>
                .orElseGet(() -> ResponseEntity.notFound().build()); // ResponseEntity<Question>
    }

    @PostMapping
    ResponseEntity createQuestion(@RequestBody NewQuestion body) {
        this.questionService.createQuestion(body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/{id}/answer")
    ResponseEntity<AnswerResultRepresentation> tryAnswer(@PathVariable("id") long id, @RequestBody UserAnswerRepresentation body) {
        return this.questionService.tryAnswerById(id, body.getAnswer())
                .map(this::createAnswerRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private AnswerResultRepresentation createAnswerRepresentation(AnswerResult answerResult) {
        final AnswerResultRepresentation result = new AnswerResultRepresentation();
        result.setResult(answerResult.toString());
        return result;
    }

}
