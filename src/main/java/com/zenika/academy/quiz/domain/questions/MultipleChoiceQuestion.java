package com.zenika.academy.quiz.domain.questions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.lang.System.lineSeparator;

public class MultipleChoiceQuestion extends AbstractQuestion {
    private final String text;
    private final List<String> suggestions;
    private final Integer correctAnswerIndex;

    public MultipleChoiceQuestion(long id, String text, List<String> incorrectSuggestions, String correctAnswer, Random random) {
        super(id);
        this.text = text;

        this.suggestions = new ArrayList<>(incorrectSuggestions);
        this.suggestions.add(correctAnswer);
        Collections.shuffle(this.suggestions, random);

        this.correctAnswerIndex = this.suggestions.indexOf(correctAnswer);
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public List<String> getSuggestions() {
        return this.suggestions;
    }

    /**
     * Try an answer.
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one (case insensitive), or if the answer is the number
     * of the right one; INCORRECT otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        return AnswerResult.fromBoolean(
                this.userAnswerIsCorrectIndex(userAnswer) || this.userAnswerIsCorrectAnswer(userAnswer)
        );
    }

    private boolean userAnswerIsCorrectAnswer(String userAnswer) {
        return userAnswer.toLowerCase().equals(this.suggestions.get(this.correctAnswerIndex).toLowerCase());
    }

    private boolean userAnswerIsCorrectIndex(String userAnswer) {
        return String.valueOf(this.correctAnswerIndex+1).equals(userAnswer);
    }
}
