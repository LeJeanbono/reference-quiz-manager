package com.zenika.academy.quiz.domain.questions;

import java.util.List;

/**
 * A question that is meant to be a part of a quiz game.
 */
public interface Question {

    /**
     * Returns the ID of the question
     */
    Long getId();

    /**
     * Returns a String that is meant to be displayed to a player of a quiz.
     */
    String getText();

    List<String> getSuggestions();

    /**
     * Tries an answer and returns the status of the answer.
     *
     * @param userAnswer the answer as provided by the player.
     *
     * @return CORRECT if the answer is the right answer, ALMOST_CORRECT if the answer is
     * almost the right one (exact meaning depends on the implementation), INCORRECT otherwise.
     */
    AnswerResult tryAnswer(String userAnswer);
}
