package com.zenika.academy.quiz.domain.questions;

import java.util.List;

public abstract class AbstractQuestion implements Question {
    private Long id;

    private String text;

    private List<String> suggestions;

    AbstractQuestion(long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

}
