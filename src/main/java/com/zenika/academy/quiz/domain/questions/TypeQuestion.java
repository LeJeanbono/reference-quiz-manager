package com.zenika.academy.quiz.domain.questions;

public enum TypeQuestion {
    OPEN,
    TRUE_FALSE,
    MULTIPLE
}
