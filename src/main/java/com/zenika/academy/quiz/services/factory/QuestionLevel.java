package com.zenika.academy.quiz.services.factory;

public enum QuestionLevel {
    EASY, MEDIUM, HARD
}
