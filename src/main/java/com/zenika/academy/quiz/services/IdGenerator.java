package com.zenika.academy.quiz.services;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

@Component
public class IdGenerator {
    private final AtomicLong counter = new AtomicLong();

    public long generateNewId() {
        return this.counter.incrementAndGet();
    }
}
