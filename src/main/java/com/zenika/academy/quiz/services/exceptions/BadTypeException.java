package com.zenika.academy.quiz.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadTypeException extends RuntimeException {
    public BadTypeException(String s) {
        super(s);
    }
}
