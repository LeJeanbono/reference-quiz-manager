package com.zenika.academy.quiz.services;

import com.zenika.academy.quiz.controllers.representation.NewQuestion;
import com.zenika.academy.quiz.domain.questions.*;
import com.zenika.academy.quiz.repositories.QuestionRepository;
import com.zenika.academy.quiz.services.exceptions.BadTypeException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class QuestionService {
    private QuestionRepository questionRepository;
    private IdGenerator idGenerator;
    private Random random;

    public QuestionService(
            QuestionRepository questionRepository,
            IdGenerator idGenerator,
            Random random

    ) {
        this.questionRepository = questionRepository;
        this.idGenerator = idGenerator;
        this.random = random;
    }

    public List<Question> getAllQuestions() {
        return this.questionRepository.getAll();
    }

    public Optional<Question> getOneQuestion(long id) {
        return this.questionRepository.getOne(id);
    }

    public Optional<AnswerResult> tryAnswerById(long id, String answer) {
        return this.questionRepository.getOne(id)
                .map(q -> ((Question) q).tryAnswer(answer));
    }

    @PostConstruct
    void initializeTestData() {
        this.questionRepository.save(new OpenQuestion(
                this.idGenerator.generateNewId(),
                "Comment s'appelle le chien d'Obélix ?",
                "Idéfix"
        ));
    }

    public void createQuestion(NewQuestion body) {
        Question question;
        switch (body.getType()) {
            case OPEN:
                question = new OpenQuestion(
                        this.idGenerator.generateNewId(),
                        body.getEnonce(),
                        body.getReponse().toString()
                );
                break;
            case MULTIPLE:
                List<String> incorrectSuggestions = new ArrayList<>(body.getSuggestions());
                incorrectSuggestions.remove(body.getReponse().toString());
                question = new MultipleChoiceQuestion(
                        this.idGenerator.generateNewId(),
                        body.getEnonce(),
                        incorrectSuggestions,
                        body.getReponse().toString(),
                        this.random
                );
                break;
            case TRUE_FALSE:
                question = new TrueFalseQuestion(
                        this.idGenerator.generateNewId(),
                        body.getEnonce(),
                        (Boolean) body.getReponse()
                );
                break;
            default:
                throw new BadTypeException("Mauvais type de question");
        }
        this.questionRepository.save(question);
    }
}
